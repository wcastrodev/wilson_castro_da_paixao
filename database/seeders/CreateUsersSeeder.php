<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class CreateUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
               'name'=>'Thiago',
               'email'=>'admin@moraesvelleda.com.br',
               'is_admin'=>'1',
               'password'=> '123123123',
            ],
            [
               'name'=>'Wilson',
               'email'=>'usuario@moraesvelleda.com.br',
               'is_admin'=>'0',
               'password'=> '123123123',
            ],
        ];

        foreach ($users as $user) {
            User::create($user);
        }
    }
}
