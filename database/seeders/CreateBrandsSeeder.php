<?php

namespace Database\Seeders;

use App\Models\Brand;
use Illuminate\Database\Seeder;

class CreateBrandsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = [
            [
                'name'=>'Volkswagen',
            ],
            [
                'name'=>'Fiat',
            ],
            [
                'name'=>'Ford',
            ],
        ];

        foreach ($brands as $brand) {
            Brand::create($brand);
        }
    }
}
