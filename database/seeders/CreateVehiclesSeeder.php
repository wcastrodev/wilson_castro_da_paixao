<?php

namespace Database\Seeders;

use App\Models\Vehicle;
use Illuminate\Database\Seeder;

class CreateVehiclesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vehicles = [
            [
               'brand_id' => 1,
               'name' => 'Gol',
               'year_manufacture' => 2020,
               'year_model' => 2020,
               'number_ports' => 2,
               'observation' => "Ótimo Veículo",
            ],
            [
               'brand_id' => 3,
               'name' => 'Focus',
               'year_manufacture' => 2015,
               'year_model' => 2015,
               'number_ports' => 4,
               'observation' => "Bom Veículo",
            ],
            [
               'brand_id' => 2,
               'name' => 'Uno',
               'year_manufacture' => 2010,
               'year_model' => 2010,
               'number_ports' => 4,
               'observation' => "Excelente Veículo",
            ],
            [
               'brand_id' => 2,
               'name' => 'Mobi',
               'year_manufacture' => 2021,
               'year_model' => 2021,
               'number_ports' => 4,
               'observation' => "Novo Veículo da Fiat",
            ],
            [
               'brand_id' => 1,
               'name' => 'Up',
               'year_manufacture' => 2021,
               'year_model' => 2021,
               'number_ports' => 4,
               'observation' => "Novo Veículo",
            ],
            [
               'brand_id' => 3,
               'name' => 'Fiesta',
               'year_manufacture' => 2010,
               'year_model' => 2010,
               'number_ports' => 2,
               'observation' => "Bom Veículo",
            ],
        ];

        foreach ($vehicles as $vehicle) {
            Vehicle::create($vehicle);
        }
    }
}
