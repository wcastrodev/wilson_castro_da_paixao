<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\UserController;

Route::get('/', function() {
	return view('app.login.index');
});

Route::name('admin.')->prefix('admin')->middleware('auth')->group(function () {
    Route::resource('veiculos', VehicleController::class);
    Route::resource('usuarios', UserController::class)->middleware('admin');
});

Route::prefix('/')->group(function () {
    Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
    Route::post('login', [LoginController::class, 'login']);
    Route::post('logout', [LoginController::class, 'logout'])->name('logout');
});

