## Atividades Realizadas

Todas as atividades foram realizadas conforme a descrição do Teste.
- Criação do CRUD de Usuário
- Criação do CRUD de Veículo
- Autenticação de Múltiplos Usuários

Alguns pontos importantes das atividades realizadas:
- A validação foi feita utilizando "Requests" para tornar o código reutilizável, ao ínves de utilizar o método "Validate" do Laravel.
- A página inicial é redicionada para a página de "Login".
- O usuário ao se logar é redirecionado para listagem de Veículos.
- O usuário do tipo "Administrador" tem acesso a todas funcionalidades.
- O usuário do tipo "Usuário" não tem acesso ao CRUD de usuários, somente tem acesso ao CRUD para cadastro de veículos. (Foi criada a restrição para demonstrar autenticação de usuários).
- Na barra de navegação do painel foi adicionado do lado direito o "Nome do Usuário", "Tipo de Usuário" e a opção de "Sair".

Os usuários criados utilizando Seeders foram os seguintes:
Usuário do Tipo "Administrador":
admin@moraesvelleda.com.br

Usuário do Tipo "Usuário":
usuario@moraesvelleda.com.br