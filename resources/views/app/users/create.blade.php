@extends('layouts.app')
@section('title', 'Criar novo usuário')


@include('layouts.navbar')

@section('content')
    <div class="container-fluid">

        <div class="col-12 mt-3">
            <h3 class="m-0 p-0">Criar novo usuário</h3>
        </div>

        @include('partials.messages.success')
        @include('partials.messages.errors')

        <div class="col-12">
          <form class="mt-4" method="POST" action="{{ route('admin.usuarios.store') }}">
            @csrf
            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="name">Nome</label>
                <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}">
                @error('name')
                  <small class="text-danger">{{ $message }}</small>
                @enderror
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="email">E-mail</label>
                <input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}">
                @error('email')
                  <small class="text-danger">{{ $message }}</small>
                @enderror
              </div>
              <div class="form-group col-md-6">
                <label for="password">Senha</label>
                <input type="password" class="form-control" name="password" id="password">
                @error('password')
                  <small class="text-danger">{{ $message }}</small>
                @enderror
              </div>
              <div class="form-group col-md-6">
                <label for="password-confirmation">Confirmar Senha</label>
                <input type="password" class="form-control" name="password_confirmation" id="password-confirmation">
                @error('password_confirmation')
                  <small class="text-danger">{{ $message }}</small>
                @enderror
              </div>
              <div class="form-group col-md-6">
                <label for="is-admin">Tipo de usuário</label>
                <select id="is-admin" class="form-control" name="is_admin">
                  <option value="1" {{ old('is_admin') == 1 ? 'selected' : '' }}>Administrador</option>
                  <option value="0" {{ old('is_admin') == 0 ? 'selected' : '' }}>Usuário</option>
                </select>
                @error('is_admin')
                  <small class="text-danger">{{ $message }}</small>
                @enderror
              </div>
            </div>
            <button type="submit" class="btn btn-primary mt-3">Salvar</button>
          </form>
        </div>
    </div>
@endsection