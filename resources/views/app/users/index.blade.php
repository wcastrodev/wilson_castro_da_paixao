@extends('layouts.app')
@section('title', 'Todos os usuário')


@include('layouts.navbar')

@section('content')

    <div class="container-fluid">

        <div class="col-12 mt-3">
            <h3 class="m-0 p-0">Todos os usuários</h3>
        </div>

        <div class="col-12 mt-3">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nome</th>
                        <th scope="col">E-mail</th>
                        <th scope="col">Tipo de usuário</th>
                        <th scope="col">Ação</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($users as $user)
                        <tr>
                            <th scope="row">{{ $user->id }}</th>
                            <td>{{ $user->name}}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->is_admin == 1 ? "Administrador" : "Usuário" }}</td>
                            <td>
                                <a class="btn btn-sm btn-warning text-white" href="{{ route('admin.usuarios.edit', $user->id) }}">Editar</a>
                                <form action="{{ route('admin.usuarios.destroy', $user->id) }}" class="d-inline" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-danger text-white">Excluir</button>
                                </form>
                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                  <div class="mt-4">
                      {!! $users->links() !!}
                  </div>
            </div>
        </div>
    </div>

@endsection