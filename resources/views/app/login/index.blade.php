@extends('layouts.app')
@section('title', 'Login')

@section('content')
  <form class="form-signin mt-5" method="POST" action="{{ route('login') }}">
    @csrf
    
    <h1 class="h3 mb-3 font-weight-normal">Entrar</h1>

    <div>
      <label for="inputEmail" class="sr-only">E-mail</label>
      <input type="email" id="inputEmail" class="form-control @error('email') is-invalid @enderror" 
             placeholder="E-mail" name="email" autofocus
      >
      <label for="inputPassword" class="sr-only">Senha</label>
      <input type="password" id="inputPassword" class="form-control @error('password') is-invalid @enderror"
           placeholder="Senha" name="password">
          @error('email')
            <span class="invalid-feedback mb-2" role="alert">
              <strong>{{ $message }}</strong>
            </span>
          @enderror
          @error('password')
            <span class="invalid-feedback mb-2" role="alert">
              <strong>{{ $message }}</strong>
            </span>
          @enderror
    </div>
      
    <div class="checkbox mb-3">
      <label>
        <input type="checkbox" value="remember-me"> Lembrar-me
      </label>
    </div>
    <button class="btn btn-lg btn-danger btn-block" type="submit">Entrar</button>

    <p class="mt-5 mb-3 text-muted">&copy; 2021</p>
  </form>
@endsection



