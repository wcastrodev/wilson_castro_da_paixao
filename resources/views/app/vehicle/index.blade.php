@extends('layouts.app')
@section('title', 'List all cars')

@include('layouts.navbar')

@section('content')

    <div class="container-fluid">

        <div class="col-12 mt-3">
            <h3 class="m-0 p-0">Todos os veículos</h3>
        </div>
        
        @include('partials.messages.success')

        <div class="col-12 mt-3">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nome do veículo</th>
                        <th scope="col">Ano de fabricação</th>
                        <th scope="col">Ano de modelo</th>
                        <th scope="col">Marca</th>
                        <th scope="col">Número de portas</th>
                        <th scope="col">Observações</th>
                        <th scope="col">Ação</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($vehicles as $vehicle)
                        <tr>
                            <th scope="row">{{ $vehicle->id }}</th>
                            <td>{{ $vehicle->name}}</td>
                            <td>{{ $vehicle->year_manufacture }}</td>
                            <td>{{ $vehicle->year_model }}</td>
                            <td>{{ $vehicle->brand->name }}</td>
                            <td>{{ $vehicle->number_ports }}</td>
                            <td>{{ $vehicle->observation }}</td>
                            <td>
                                <a class="btn btn-sm btn-warning text-white" href="{{ route('admin.veiculos.edit', $vehicle->id) }}">Editar</a>
                                <a class="btn btn-sm btn-primary text-white" href="{{ route('admin.veiculos.show', $vehicle->id) }}">Detalhes</a>
                                <form action="{{ route('admin.veiculos.destroy', $vehicle->id) }}" class="d-inline" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-danger text-white">Excluir</button>
                                </form>
                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                  <div class="mt-4">
                    {!! $vehicles->links() !!}
                  </div>
            </div>
        </div>
    </div>

@endsection