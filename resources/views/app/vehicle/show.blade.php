@extends('layouts.app')
@section('title', 'Detalhes do veículo')


@include('layouts.navbar')

@section('content')

    <div class="container-fluid">

        <div class="col-12 mt-3">
            <h3 class="m-0 p-0">Detalhes do veículo</h3>
        </div>

        <div class="col-12 mt-3">

            <div class="card">
                <div class="card-body">
                 
                    <h4>{{ $vehicle->name }}</h4>
                    <hr />

                    <p>Abaixo veja as informações detalhadas do veículo</p>

                    <ul>
                        <li><b>Ano de fabricação</b>: {{ $vehicle->year_manufacture }}</li>
                        <li><b>Ano de modelo</b>: {{ $vehicle->year_model }}</li>
                        <li><b>Marca</b>: {{ $vehicle->brand->name }}</li>
                        <li><b>Número de portas</b>:  {{ $vehicle->number_ports }}</li>
                    </ul>

                    <p><b>Observações: </b></p>
                    <p>{{ $vehicle->observation }}</p>

                    <a href="{{ route('admin.veiculos.index') }}" class="btn btn-secondary mt-3">Voltar a listagem de veículos</a>
                </div>
            </div>
        </div>

    </div>

@endsection