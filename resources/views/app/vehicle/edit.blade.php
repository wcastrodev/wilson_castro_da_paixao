@extends('layouts.app')
@section('title', 'Editar veículo')


@include('layouts.navbar')

@section('content')

    <div class="container-fluid">

        <div class="col-12 mt-3">
            <h3 class="m-0 p-0">Editar veículo</h3>
        </div>

        @include('partials.messages.success')
        @include('partials.messages.errors')

        <div class="col-12">
            <form class="mt-4" method="POST" action="{{ route('admin.veiculos.update', $vehicle->id) }}">
                @csrf
                @method('PUT')
                <div class="form-row">
                  <div class="form-group col-md-12">
                    <label for="car">Nome do veículo</label>
                    <input type="text" class="form-control" name="name" id="name" value="{{ old('name', $vehicle->name) }}">
                    @error('name')
                      <small class="text-danger">{{ $message }}</small>
                    @enderror
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="Manufacture">Ano de fabricação</label>
                    <input type="text" class="form-control" id="year-manufacture" name="year_manufacture" maxlength="4" 
                           value="{{ old('year_manufacture', $vehicle->year_manufacture) }}"
                    >
                    @error('year_manufacture')
                      <small class="text-danger">{{ $message }}</small>
                    @enderror
                  </div>
                  <div class="form-group col-md-6">
                    <label for="model">Ano de modelo</label>
                    <input type="text" class="form-control" id="year-model" name="year_model" maxlength="4" 
                           value="{{ old('year_model', $vehicle->year_model) }}"
                    >
                    @error('year_model')
                      <small class="text-danger">{{ $message }}</small>
                    @enderror
                  </div>

                  <div class="form-group col-md-12">
                    <label for="brand">Marca</label>
                    <select id="brand-id" name="brand_id" class="form-control">
                      @foreach($brands as $brand)
                        <option value="{{ $brand->id }}" {{ old('brand_id',  $brand->id) == $brand->id ? 'selected' : '' }}>
                          {{ $brand->name }}
                        </option>
                      @endforeach
                    </select>
                    @error('brand_id')
                      <small class="text-danger">{{ $message }}</small>
                    @enderror
                  </div>

                  <div class="orm-group col-12 mb-3">
                        <label for="modelCar">Número de portas</label> 
                        @for($i = 2;$i <= 4;$i++)
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="number_ports" id="{{ $i }}ports" value="{{ $i }}"   
                                {{ old('number_ports') == $i ? 'checked' : '' }}
                                {{ $vehicle->number_ports === $i && !old('number_ports') ? 'checked' : '' }}
                            >
                            <label class="form-check-label" for="{{ $i }}ports">
                                {{ $i }} portas
                            </label>
                        </div>
                        @endfor
                        <div>
                            @error('number_ports')
                              <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="observation">Observações</label>
                        <textarea rows="3" class="form-control " name="observation" id="observation">{{ old('observation', $vehicle->observation) }}</textarea>
                        @error('observation')
                          <small class="text-danger">{{ $message }}</small>
                        @enderror
                      </div>
                </div>

                <button type="submit" class="btn btn-primary mt-3">Editar</button>
                <a href="{{ route('admin.veiculos.index') }}" class="btn btn-secondary mt-3">Voltar a listagem de veículos</a>
              </form>
        </div>

    </div>

@endsection