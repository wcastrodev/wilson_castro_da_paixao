
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navNavigation" aria-controls="navNavigation" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navNavigation">
        <a class="navbar-brand" href="#"><img src="{{ asset('mv_logo.png') }}" width="120" alt=""></a>
        <ul class="navbar-nav float-left mr-auto mt-2 mt-lg-0">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Veículos
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{ route('admin.veiculos.create') }}">Novo veículo</a>
                  <a class="dropdown-item" href="{{ route('admin.veiculos.index') }}">Listar veículos</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Usuários
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  @if(Auth::user()->is_admin == 1)
                          <a class="dropdown-item" href="{{ route('admin.usuarios.create') }}">Novo usuário</a>
                  @endif
                  <a class="dropdown-item" href="{{ route('admin.usuarios.index') }}">Listar usuários</a>
                </div>
            </li>
            
        </ul>
        <div class="float-right mr-5">
            <strong>{{ Auth::user()->name }} ({{ Auth::user()->is_admin == 1 ? "Administrador" : "Usuário" }})</strong>  | 
            <a href="{{ route('logout') }}" class="px-1"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <span class="text-base font-weight-bold">Sair</span>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </div>
    </div>
</nav>