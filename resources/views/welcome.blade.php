@extends('layouts.app')
@section('title', 'Wilson Castro da Paixão')

@section('content')

    <div class="container">

        <div class="col-12 mt-3">
            <h3 class="m-0 p-0">Wilson Castro da Paixão</h3>
        </div>

        <div class="card col-12 mt-3">
          <div class="card-body">
            <h5 class="card-title">Resumo</h5>
            <p class="card-text">
              O teste consiste na criação de dois <b>CRUD</b> básicos e de um <b> modulo de autenticação </b> que deve "proteger" o CRUD de acessos não autorizados.<br>
              O foco principal do teste esta no <b>back-end</b>, por isso a estrutura do layout com todos os arquivos blades já estão inclusos neste repositorio, 
              sendo necessário apenas a montagem das rotas e links para navageção dentro do sistema utlizando os blades fornecidos. <br>
              O canditado pode desenvolver uma interface mais elaborada caso desejar utilizando qualquer biblioteca ou framweork front-end que desejar. (passa a ser considerado como um diferêncial.) <br>
              Deve ser criado um CRUD para cadastro de veiculos utilizando a view <b>vehicle</b> como formulário e uma outra CRUD para o cadastro de usuários com a view <b>users</b>, a tela de login deve ser feita utilizando a view <b>login</b>.</p>  
            <p class="card-text">
              <b>Pontos importantes:</b>  <br>
              - Toda a esturutra do banco da dados deve ser feita utilizando as <b>migrations</b> do Laravel. <br>
              - O cadastro de veículos deve possuir um relacionamento com uma tabela de marcas de veiculo. (criar 3 marcas "fictícias") <br>
              - O modulo de autenticação deve ser feito utilizando o <b>Artisan</b> e o <b>Auth</b> do Laravel, e deve ter dois tipos de usuários (admin e usuario) <br>
              - O primeiro acesso ao sistema, os tipos de acesso e as marcas de veiculos devem serem criadas utilizando <b>Seeders</b> <br>
              - As <b>rotas</b> devem estar configuradas e a navegação estabelecida entre as <b>Views</b> funcionando corretamente.  <br>
              - Todos os formularios devem possuir autenticação back-end com o <b>Validate</b>.<br>
              - <b>Editar o titulo do sistema colocando seu nome no lugar de "Teste Laravel".</b>
            </p>

            <p><i>Realize todas as atividades que conseguir, porém caso não saiba, não é necessário concluír todas. (<b>Exemplo:</b> Se não souber pode como fazer, pode realizar o teste sem o módulo de login). A avaliação do candidato é realizada com base em quais atividades solicitadas o candidato conseguiu concluír e de qual forma ele chegou ao resultado final, para verificarmos o perfil do candidato e a qual vaga pode estar apto.</i></p>

            <p style="font-weight: bold;">Durante o teste a consulta a documentação do Laravel é permitida e recomendada.</p>
            <a href="https://laravel.com/docs/8.x" target="_blank" class="btn btn-primary self-align-right">Documentação Laravel 8.x</a>
          </div>
        </div>

        <div class="card col-12 mt-3">
          <div class="card-body">
            <h5 class="card-title">Atividades</h5>
            <ol class="list-group list-group-numbered w-100">
              <li class="list-group-item d-flex justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                  <div class="fw-bold">Artisan</div>
                  <ul>
                    <li>Elaborar o banco de dados.</li>
                    <li>Elaborar Seeders de acesso e relacionamento.</li>
                    <li>Elaborar as Models do sistema.</li>
                    <li>Elaborar as Controllers.</li>
                  </ul>
                </div>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                  <div class="fw-bold">Routes/Views</div>
                  <ul>
                    <li>Configurar as rotas das Views.</li>
                    <li>Configurar os links de navegação entre as Views.</li>
                  </ul>
                </div>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                  <div class="fw-bold">Controller</div>
                  <ul>
                    <li>Elaborar o back-end dos formulários e seus métodos</li>
                    <li>Configurar a validação de campos com o Validate.</li>
                    <li>Elaboração dos métodos dos CRUDs (show, create, insert, edit, update e delete)</li>
                  </ul>
                </div>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                  <div class="fw-bold">GIT</div>
                  <ul>
                    <li>Commitar seu projeto e efetuar o push para uma nova branch com seu nome (nome_sobrenome)</li>
                  </ul>
                </div>
              </li>
            </ol>
          </div>
        </div>
    </div>

@endsection



