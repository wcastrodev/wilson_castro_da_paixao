@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
        <strong>{{ $message }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif