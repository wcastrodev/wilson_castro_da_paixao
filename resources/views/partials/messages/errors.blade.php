@if ($errors->any())
    @if (count($errors) > 1)
    <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
        <strong>Oops!</strong> Ocorreu alguns erros ao realizar o cadastro.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @else
    <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
        <strong>Oops!</strong> Ocorreu um erro ao realizar o cadastro.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
@endif