<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory;

    protected $fillable = [
        'brand_id',
        'name',
        'year_manufacture',
        'year_model',
        'number_ports',
        'observation',
    ];

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
}
