<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VehicleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'year_manufacture' => 'required|integer|between:1950,' . date("Y"),
            'year_model' => 'required|integer|between:1950,' . date("Y"),
            'brand_id' => 'required|integer',
            'number_ports' => 'required|integer|between:2,4',
            'observation' => 'required|string'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'O campo "Nome do veículo" é obrigatório',
            'year_manufacture.required' => 'O campo "Ano de fabricação" é obrigatório',
            'year_model.required' => 'O campo "Ano de modelo" é obrigatório',
            'brand_id.required' => 'O campo "Marca" é obrigatório',
            'number_ports.required' => 'O campo "Número de portas" é obrigatório',
            'observation.required' => 'O campo "Observações" é obrigatório',
            'name.max' => 'O campo "Nome do veículo" não pode ser superior a :max caracteres',
            'year_manufacture.between' => 'O campo "Ano de fabricação" deve ser entre :min e :max.',
            'year_model.between' => 'O campo "Ano de modelo" deve ser entre :min e :max.',
            'number_ports.between' => 'O campo "Número de portas" deve ser entre :min e :max.'
        ];
    }
}
