<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . request()->segment(3),
            "is_admin" => 'required|integer',
            'password' => 'required|string|confirmed|min:8|max:255',
            'password_confirmation' => 'required|min:8'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'O campo "Nome" é obrigatório',
            'email.required' => 'O campo "E-mail" é obrigatório',
            'is_admin.required' => 'O campo "Tipo de usuário" é obrigatório',
            'password.required' => 'O campo "Senha" é obrigatório',
            'password_confirmation.required' => 'O campo "Confirmar Senha" é obrigatório',
            'email.email' => 'O campo "E-mail" deve ser um endereço de e-mail válido.',
            'email.unique' => 'O campo "E-mail" já está sendo utilizado.',
            'name.max' => 'O campo "Nome" não pode ser superior a :max caracteres',
            'password.max' => 'O campo "Nome" não pode ser superior a :max caracteres',
            'password_confirmation.max' => 'O campo "Confirmar Senha" não pode ser superior a :max caracteres',
            'password.min' => 'O campo "Senha" deve ser pelo menos :min.',
            'password_confirmation.min' => 'O campo "Confirmar Senha" deve ser pelo menos :min.',
        ];
    }
}
